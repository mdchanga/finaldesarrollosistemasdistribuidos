class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hover: false,
            form: {
                correo: "",
                contrasenna: ""
            }
        };
    }
    onClick = () => {
        const { history } = this.props;
        fetch("http://127.0.0.1:8080/api/logear", {
            method: "POST",
            headers: {
                'Accept': 'application/json; charset=utf-8',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state.form)
        }).then(function (response) {
            return Promise.all([response.ok, response.json()])
        }).then(function ([responseOk, data]) {
            console.clear();
            if(responseOk){
                history.push('/usuario/'+data)
            }
            console.log(data);
        })
    }
    onMouseEnter = () => {
        this.setState({
            hover: true
        })
    }
    onMouseLeave = () => {
        this.setState({
            hover: false
        })
    }
    handleInputChange = (event) => {
        const { name, value } = event.target;
        this.setState(prevState => ({
                form: {
                    ...prevState.form,
                    [name]: value
                }
            })
        );
    }
    render() {
        const divFormLoginStyle = {
            background: "rgb(241, 247, 252)",
            height: "100vh",
            padding: "80px 0"
        }
        const formLoginStyle = {
            backgroundColor: "white",
            boxShadow: "1px 1px 5px rgba(0,0,0,0.1)",
            margin: "0 auto",
            padding: "40px",
            width: "320px"
        }
        const illustrationFormLoginStyle = {
            padding: "0 0 20px"
        }
        const iconFormLoginStyle = {
            color: "rgb(244,175,71)",
            fontSize: "100px"
        };
        const nameFormLoginStyle = {
            color: "rgb(244,175,71)"
        };
        const inputFormLoginStyle = {
            background: "#f7f9fc",
            border: "none",
            borderBottom: "1px solid rgb(223, 231, 241)",
            borderRadius: "0px",
            height: "42px",
            textIndent: "8px"
        }
        const buttonFormLoginStyle = {
            background: !this.state.hover ? "rgb(244,175,71)" : "rgb(220,158,64)",
            color: "white",
            marginTop: "26px",
            padding: "11px"
        }
        const hrefFormLoginStyle = {
            color: "rgb(125, 135, 145)",
            display: "block",
            fontSize: "12px",
            textAlign: "center"
        }
        return (
            <div style={divFormLoginStyle}>
                <form style={formLoginStyle}>
                    <div style={illustrationFormLoginStyle}>
                        <div className="d-flex justify-content-center">
                            <i className="icon ion-ios-navigate" style={iconFormLoginStyle}></i>
                        </div>
                        <div className="d-flex justify-content-center">
                            <h1 style={nameFormLoginStyle}>EatDate</h1>
                        </div>
                    </div>
                    <div className="form-group">
                        <input className="form-control" type="email" name="correo" placeholder="Email" style={inputFormLoginStyle} value={this.state.form.correo} onChange={this.handleInputChange}></input>
                    </div>
                    <div className="form-group">
                        <input className="form-control" type="password" name="contrasenna" placeholder="Contraseña" style={inputFormLoginStyle} value={this.state.form.contrasenna} onChange={this.handleInputChange}></input>
                    </div>
                    <div className="form-group">
                        <button className="btn btn-warning btn-block" type="button" onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave} style={buttonFormLoginStyle} onClick={this.onClick}>
                            Ingresar
                        </button>
                    </div>
                    <Link to="/registrar" style={hrefFormLoginStyle}>¿Aún no estás registrado?</Link>
                </form>
            </div>
        );
    }
}