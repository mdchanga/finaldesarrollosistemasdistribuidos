package com.upc.serviciosREST.repositorio;

import com.upc.serviciosREST.entidades.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepositorio extends CrudRepository<Usuario,Long> {
    @Query("SELECT i FROM Usuario i WHERE i.correo=:correo AND i.contrasenna=:contrasenna")
    public Usuario buscarUsuario(String correo, String contrasenna);
}
