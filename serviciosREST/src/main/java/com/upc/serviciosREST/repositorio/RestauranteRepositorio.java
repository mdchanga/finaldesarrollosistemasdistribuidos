package com.upc.serviciosREST.repositorio;

import com.upc.serviciosREST.entidades.Restaurante;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RestauranteRepositorio extends CrudRepository<Restaurante,Long> {
}
