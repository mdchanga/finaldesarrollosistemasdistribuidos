package com.upc.serviciosREST.negocio;

import com.upc.serviciosREST.entidades.Invitacion;
import com.upc.serviciosREST.entidades.InvitacionId;
import com.upc.serviciosREST.repositorio.InvitacionRespositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InvitacionNegocio {

    @Autowired
    private InvitacionRespositorio invitacionRepositorio;
    @Autowired
    private InvitacionNegocio invitacionNegocio;

    public Invitacion grabar(Invitacion invitacion){
        return invitacionRepositorio.save(invitacion);
    }

    public List<Invitacion> buscarInvitacionesInvitadores(Long codigo){
        List<Invitacion> invitaciones = invitacionRepositorio.buscarInvitacionesInvitadores(codigo);
        for(Invitacion invitacion: invitaciones){
            invitacion.getUsuario().setInvitaciones(new ArrayList<>());
            invitacion.getUsuarioInvitado().setInvitaciones(new ArrayList<>());
            invitacion.getRestaurante().setInvitaciones(new ArrayList<>());
        }
        return invitaciones;
    }
    public List<Invitacion> buscarInvitacionesEnviadas(Long codigo){
        List<Invitacion> invitaciones = invitacionRepositorio.buscarInvitacionesEnviadas(codigo);
        for(Invitacion invitacion: invitaciones){
            invitacion.getUsuario().setInvitaciones(new ArrayList<>());
            invitacion.getUsuarioInvitado().setInvitaciones(new ArrayList<>());
            invitacion.getRestaurante().setInvitaciones(new ArrayList<>());
        }
        return invitaciones;
    }
    public Invitacion actualizar(InvitacionId invitacionId) {
        Invitacion i = obtenerInvitacion(invitacionId);
        i.setEstado("aceptado");
        i.getUsuario().setInvitaciones(new ArrayList<>());
        i.getUsuarioInvitado().setInvitaciones(new ArrayList<>());
        i.getRestaurante().setInvitaciones(new ArrayList<>());
        if (i != null) {
            return invitacionRepositorio.save(i);
        } else {
            return null;
        }
    }
    public Invitacion borrarInvitacion(InvitacionId invitacionId){
        Invitacion invitacion = obtenerInvitacion(invitacionId);
        invitacion.getUsuario().setInvitaciones(new ArrayList<>());
        invitacion.getUsuarioInvitado().setInvitaciones(new ArrayList<>());
        invitacion.getRestaurante().setInvitaciones(new ArrayList<>());
        if (invitacion!=null){
            invitacionRepositorio.delete(invitacion);
        }
        return invitacion;
    }

    private Invitacion obtenerInvitacion(InvitacionId invitacionId){
        return invitacionRepositorio.buscar(invitacionId);
    }
}