package com.upc.serviciosREST.negocio;

import com.upc.serviciosREST.entidades.Invitacion;
import com.upc.serviciosREST.entidades.Usuario;
import com.upc.serviciosREST.repositorio.UsuarioRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsuarioNegocio {
    @Autowired
    private UsuarioRepositorio usuarioRepositorio;

    public Usuario grabar(Usuario usuario){
        Usuario usuarioConCoordenadas = new Usuario();
        usuarioConCoordenadas.setNickname(usuario.getNickname());
        usuarioConCoordenadas.setSexo(usuario.getSexo());
        usuarioConCoordenadas.setCorreo(usuario.getCorreo());
        usuarioConCoordenadas.setContrasenna(usuario.getContrasenna());
        usuarioConCoordenadas.setLatitud(crearCoordenadaAleatoria());
        usuarioConCoordenadas.setLongitud(crearCoordenadaAleatoria());
        usuarioConCoordenadas.setCodigo(usuarioRepositorio.save(usuarioConCoordenadas).getCodigo());
        return sincontrasenna(usuarioConCoordenadas);
    }
    public List<Usuario> grabar(List<Usuario> usuarios){
        List<Usuario> usuariosConCoordenadas = new ArrayList<>();
        for(Usuario usuario: usuarios ){
            usuariosConCoordenadas.add(grabar(usuario));
        }
        return usuariosConCoordenadas;
    }
    public Usuario buscarUsuario(String correo, String contrasenna){
        return usuarioRepositorio.buscarUsuario(correo,contrasenna);
    }
    public Usuario buscarUsuarioSinContrasenna(Long codigo){
        Usuario usuario = buscarUsuario(codigo);
        usuario=sincontrasenna(usuario);
        return usuario;
    }
    public List<Usuario> buscarUsuariosRadio(Long codigo, double radio, String sexo) {
        Usuario usuarioCliente = buscarUsuario(codigo);
        List<Usuario> usuarios = buscarUsuarios();
        List<Usuario> usuariosRadio = new ArrayList<>();
        for(Usuario usuario: usuarios){
            if(!usuario.getCodigo().equals(codigo)){
                if(calcularDistanciaGeografica(usuarioCliente.getLatitud(),usuarioCliente.getLongitud(),usuario.getLatitud(),usuario.getLongitud())<=radio){
                    if(usuario.getSexo().equals(sexo) || sexo.equals("HM")){
                        usuariosRadio.add(sincontrasenna(usuario));
                    }
                }
            }
        }
        return  usuariosRadio;
    }

    private Usuario sincontrasenna(Usuario usuario){
        Usuario nuevoUsuario = new Usuario();
        nuevoUsuario.setCodigo(usuario.getCodigo());
        nuevoUsuario.setNickname(usuario.getNickname());
        nuevoUsuario.setSexo(usuario.getSexo());
        nuevoUsuario.setCorreo(usuario.getCorreo());
        nuevoUsuario.setContrasenna(usuario.getContrasenna());
        nuevoUsuario.setLatitud(usuario.getLatitud());
        nuevoUsuario.setLongitud(usuario.getLongitud());
        nuevoUsuario.setContrasenna("");
        List<Invitacion> nuevasInvitaciones = usuario.getInvitaciones();
        if(nuevasInvitaciones!=null){
            for(Invitacion invitacion: nuevasInvitaciones){
                invitacion.getUsuario().setInvitaciones(new ArrayList<>());
                invitacion.getUsuarioInvitado().setInvitaciones(new ArrayList<>());
                invitacion.getRestaurante().setInvitaciones(new ArrayList<>());
            }
            nuevoUsuario.setInvitaciones(nuevasInvitaciones);
        }
        return nuevoUsuario;
    }
    private Usuario buscarUsuario(Long codigo){
        return usuarioRepositorio.findById(codigo).get();
    }
    private List<Usuario> buscarUsuarios(){
        return (List<Usuario>) usuarioRepositorio.findAll();
    }
    private double crearCoordenadaAleatoria(){
        return ((Math.random() * ((1d - (-1d)) + 1d)) - 1d)/64d;
    }
    private double calcularDistanciaGeografica(double latitud1, double longitud1, double latitud2, double longitud2){
        double radioTierra = 6378.1d;
        latitud1=latitud1*Math.PI/180;
        longitud1=longitud1*Math.PI/180;
        latitud2=latitud2*Math.PI/180;
        longitud2=longitud2*Math.PI/180;
        double variacionLatitud = latitud2-latitud1;
        double variacionLongitud = longitud2-longitud1;
        double a = Math.pow(Math.sin(variacionLatitud/2),2)+Math.cos(latitud1)*Math.cos(latitud2)*Math.pow(Math.sin(variacionLongitud/2),2);
        double c = 2*Math.atan2(Math.sqrt(a),Math.sqrt(1-a));
        return radioTierra*c;
    }
}
