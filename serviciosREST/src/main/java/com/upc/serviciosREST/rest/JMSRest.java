package com.upc.serviciosREST.rest;

import com.upc.serviciosREST.jms.JMSProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api")
public class JMSRest {
    @Autowired
    private JMSProductor jmsProductor;

    @GetMapping("/imageServer")
    public boolean imageServer() {
        try {
            jmsProductor.send("");
            return true;
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Servidor de Imágenes No Disponible");
        }
    }
}